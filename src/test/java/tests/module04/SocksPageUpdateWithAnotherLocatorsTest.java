package tests.module04;

import org.junit.Test;
import pages.module04.*;
import tests.AbstractTest;

public class SocksPageUpdateWithAnotherLocatorsTest extends AbstractTest{
    SocksUpdateWithLocatorsClassesPage socksUpdateWithLocatorsClassesPage;
    SocksUpdateWithLocatorsTagsAndClassesPage socksUpdateWithLocatorsTagsAndClassesPage;
    SocksUpdateWithLocatorsIDPage socksUpdateWithLocatorsIDPage;
    SocksUpdateWithLocatorsAllPage socksUpdateWithLocatorsAllPage;


    private void getPage(){
        driver.get("http://qajava.skillbox.ru/module04/homework/auth/changed.html");
    }

    @Test
    public void errorMessageWithLocatorsClassesTest(){
        socksUpdateWithLocatorsClassesPage = new SocksUpdateWithLocatorsClassesPage(driver);
        getPage();
        socksUpdateWithLocatorsClassesPage.checkErrorMessage();
    }

    @Test
    public void errorMessageWithLocatorsTagsAndClassesTest(){
        socksUpdateWithLocatorsTagsAndClassesPage = new SocksUpdateWithLocatorsTagsAndClassesPage(driver);
        getPage();
        socksUpdateWithLocatorsTagsAndClassesPage.checkErrorMessage();
    }

    @Test
    public void errorMessageWithLocatorsIDTest(){
        socksUpdateWithLocatorsIDPage = new SocksUpdateWithLocatorsIDPage(driver);
        getPage();
        socksUpdateWithLocatorsIDPage.checkErrorMessage();
    }

    @Test
    public void errorMessageWithAllLocatorsIDTest(){
        socksUpdateWithLocatorsAllPage = new SocksUpdateWithLocatorsAllPage(driver);
        getPage();
        socksUpdateWithLocatorsAllPage.checkErrorMessage();
    }
}
