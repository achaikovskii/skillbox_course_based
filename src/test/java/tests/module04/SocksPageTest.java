package tests.module04;

import org.junit.Test;
import pages.module04.SocksPage;
import tests.AbstractTest;

public class SocksPageTest extends AbstractTest {
    SocksPage socksPage;

    private void getPage(){
        driver.get("http://qajava.skillbox.ru/module04/homework/auth/index.html");
    }

    @Test
    public void errorMessageTest(){
        socksPage = new SocksPage(driver);
        getPage();
        socksPage.checkErrorMessage();
    }
}
