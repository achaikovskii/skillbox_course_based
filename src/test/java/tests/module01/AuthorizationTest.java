package tests.module01;

import jdk.jfr.Description;
import org.junit.Test;
import pages.module01.AuthorizationPage;

public class AuthorizationTest extends AbstractTest {
    AuthorizationPage authorizationPage;
    @Description("Регистрация на сайте")
    @Test
    public void registrationTest(){
        authorizationPage = new AuthorizationPage(driver);
        authorizationPage.inputAuthorizationForm();
    }

    @Description("Успешная регистрация на стайте")
    @Test
    public void successRegistrationTest(){
        authorizationPage = new AuthorizationPage(driver);
        registrationTest();
        authorizationPage.checkMessageInSuccessMessage();
    }

    @Description("Неуспешная регистрация")
    @Test
    public void unSuccessRegistrationWithoutEmailTest(){
        authorizationPage = new AuthorizationPage(driver);
        authorizationPage.inputAuthorizationFormWithoutEmail();
        authorizationPage.checkEmailError();
        authorizationPage.checkErrorOfFieldsEmpty();
    }
}
