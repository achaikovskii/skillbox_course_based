package tests.module01;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.module01.AuthorizationPage;

import java.util.concurrent.TimeUnit;

public class AbstractTest {
    public static WebDriver driver;

    public static void openBrowser(){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://lm.skillbox.cc/qa_tester/module05/practice2/");
    }

    public static void closeBrowser(){
        driver.close();
        driver.quit();
        driver = null;
    }


    @Before
    public void setUp() {
        openBrowser();
    }

    @After
    public void clearAll(){closeBrowser();}

}
