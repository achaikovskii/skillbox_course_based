package tests.module02;

import org.junit.Test;

public class ImtPageTest {
    private String countImt(float weightKg, float heightCm){
        float userIndex = Math.round(weightKg / Math.pow(heightCm / 100, 2));
        String userResult = null;

        if(userIndex <= 16){
            userResult = "выраженный дефицит массы тела";
        } else if (userIndex > 16 && userIndex < 19) {
            userResult = "недостаточная масса тела";
        } else if (userIndex > 19 && userIndex < 25) {
            userResult = "нормальная масса тела";
        } else if (userIndex > 25) {
            userResult = "избыточная масса тела";
        }

        if (heightCm == 0.0f) userResult += "указан некорректный рост";
        System.out.println(userIndex);
        return userResult;
    }

    @Test
    public void checkNegativeHeight(){
        assert (countImt(75, -1).contains("указан некорректный рост"));
    }

    @Test
    public void checkHeightZero(){
        assert (countImt(75, 0).contains("указан некорректный рост"));
    }

    @Test
    public void checkHeight350(){
        assert (countImt(75, 350).contains("указан некорректный рост"));
    }

    @Test
    public void checkHeight351(){
        assert (countImt(75, 351).contains("указан некорректный рост"));
    }

    @Test
    public void checkNegativeWeight(){
        assert (countImt(-1, 170).contains("указан некорректный вес"));
    }

    @Test
    public void checkWeightZero(){
        assert (countImt(0, 170).contains("указан некорректный вес"));
    }

    @Test
    public void checkWeight1000(){
        assert (countImt(1000, 170).contains("указан некорректный вес"));
    }

    @Test
    public void checkWeight1001(){
        assert (countImt(1001, 170).contains("указан некорректный вес"));
    }

    @Test
    public void checkImtNormal(){
        System.out.println(countImt(75, 180));
    }

    @Test
    public void checkImt1(){
        assert (countImt(4, 180).equals("выраженный дефицит массы тела"));
    }

    @Test
    public void checkImt16(){
        assert (countImt(53, 180).equals("выраженный дефицит массы тела"));
    }

    @Test
    public void checkImt17(){
        assert (countImt(55, 180).equals("недостаточная масса тела"));
    }

    @Test
    public void checkImt18(){
        assert (countImt(57, 180).equals("недостаточная масса тела"));
    }

    @Test
    public void checkImt19(){
        assert (countImt(60, 180).equals("нормальная масса тела"));
    }

    @Test
    public void checkImt24(){
        assert (countImt(78, 180).equals("нормальная масса тела"));
    }

    @Test
    public void checkImt25(){
        assert (countImt(80, 180)).equals("избыточная масса тела");
    }

    @Test
    public void checkImt26(){
        assert (countImt(84, 180).equals("избыточная масса тела"));
    }







}
