package tests.module02;

import jdk.jfr.Description;
import org.junit.Test;

public class CashBackTest {
    private Integer getRefundTicketPricePercent(Integer hoursBeforeConcert, Boolean wasConcertCancelled, Boolean wasConcertRescheduled) {
        if(wasConcertCancelled && wasConcertRescheduled) return 100;
        if(hoursBeforeConcert>=144 && hoursBeforeConcert<=240) return 50;
        if(hoursBeforeConcert>3 && hoursBeforeConcert<=144) return 30;
        return 0;
    }

    @Description("Проверяем первое условие")
    @Test
    public void cancelledAndRescheduledTest(){
        assert (getRefundTicketPricePercent(10, true, true).equals(100));
    }

    @Description("Во втором условии проверяем hoursBeforeConcert<=240 методом граничных значений")
    @Test
    public void test240BoundaryValues(){
        assert (getRefundTicketPricePercent(241, false, false).equals(0));
        assert (getRefundTicketPricePercent(240, false, false).equals(50));
        assert (getRefundTicketPricePercent(239, false, false).equals(50));
    }

    @Description("Во втором условии проверяем hoursBeforeConcert>=144 методом граничных значений")
    @Test
    public void testFirstCondition144BoundaryValues(){
        assert (getRefundTicketPricePercent(143, false, false).equals(30));
        assert (getRefundTicketPricePercent(144, false, false).equals(50));
        assert (getRefundTicketPricePercent(145, false, false).equals(50));
    }

    @Description("Во третьем условии проверяем hoursBeforeConcert<=144 методом граничных значений")
    @Test
    public void testSecondCondition144BoundaryValues(){
        assert (getRefundTicketPricePercent(145, false, false).equals(50));
        assert (getRefundTicketPricePercent(144, false, false).equals(30));
        assert (getRefundTicketPricePercent(143, false, false).equals(30));
    }

    @Description("Во третьем условии проверяем hoursBeforeConcert>3 методом граничных значений")
    @Test
    public void test3BoundaryValues(){
        assert (getRefundTicketPricePercent(2, false, false).equals(0));
        assert (getRefundTicketPricePercent(3, false, false).equals(0));
        assert (getRefundTicketPricePercent(4, false, false).equals(30));
    }

}
