package tests.module02;

import jdk.jfr.Description;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.module02.WelcomePage;

import java.util.concurrent.TimeUnit;

public class WelcomePageTest{
    public static WebDriver driver;
    WelcomePage welcomePage;

    public static void openBrowser(){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://lm.skillbox.cc/qa_tester/module01/");
    }

    @Description("Тест проверяющий текст сообщения при входе")
    @Test
    public void welcomeMessageTest(){
        welcomePage = new WelcomePage(driver);
        welcomePage.goToWelcome();
        welcomePage.checkWelcomeString();
    }

    @Description("Тест проверяющий привествие с пустой строкой")
    @Test
    public void welcomeMessageWithoutTextTest(){
        welcomePage = new WelcomePage(driver);
        welcomePage.checkWelcomeStringWithoutTextInField();

    }

    public static void closeBrowser(){
        driver.close();
        driver.quit();
        driver = null;
    }

    @Before
    public void setUp() {
        openBrowser();
    }

    @After
    public void clearAll(){closeBrowser();}
}
