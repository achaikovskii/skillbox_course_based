package tests.module02;

import jdk.jfr.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.module02.WelcomePage;
import pages.module02.WelcomePageAdvanced;

import java.util.concurrent.TimeUnit;

public class WelcomePageAdvancedTest {
    public static WebDriver driver;
    WelcomePageAdvanced welcomePageAdvanced;

    public static void openBrowser(){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://lm.skillbox.cc/qa_tester/module02/homework1/");
    }

    @Description("Проверки с полным заполнением")
    @Test
    public void setFullFieldsTest(){
        welcomePageAdvanced = new WelcomePageAdvanced(driver);
        welcomePageAdvanced.setWithFullData();
        welcomePageAdvanced.checkFullField();
    }

    @Description("Проверки без имени")
    @Test
    public void setWithoutNameTest(){
        welcomePageAdvanced = new WelcomePageAdvanced(driver);
        welcomePageAdvanced.setWithoutName();
        welcomePageAdvanced.checkNameFieldEmpty();
    }

    @Description("Проверки без email")
    @Test
    public void setWithoutEmailTest(){
        welcomePageAdvanced = new WelcomePageAdvanced(driver);
        welcomePageAdvanced.setWithoutEmail();
        welcomePageAdvanced.checkEmailFieldEmpty();
    }

    @Description("Проверки без телефона")
    @Test
    public void setWithoutPhoneTest(){
        welcomePageAdvanced = new WelcomePageAdvanced(driver);
        welcomePageAdvanced.setWithoutPhone();
        welcomePageAdvanced.checkPhoneFieldEmpty();
    }

    @Description("")
    @Test
    public void setNoDataTest(){
        welcomePageAdvanced = new WelcomePageAdvanced(driver);
        welcomePageAdvanced.setNoData();
        welcomePageAdvanced.checkEmptyAllData();
    }


    public static void closeBrowser(){
        driver.close();
        driver.quit();
        driver = null;
    }

    @Before
    public void setUp() {
        openBrowser();
    }

    @After
    public void clearAll(){closeBrowser();}
}
