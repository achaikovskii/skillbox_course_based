package tests.module02;

import org.junit.Test;

public class ScoreTransfer {
    private String getMarkResult(Integer mark) {
        if (mark >= 0 && mark <= 35) return "2";
        if (mark > 35 && mark <= 56) return "3";
        if (mark > 56 && mark < 71) return "4";
        if (mark > 72 && mark < 100) return "5";

        return "no mark result";
    }

    @Test
    public void checkScore101(){
        assert (getMarkResult(101).equals("no mark result"));
    }

    @Test
    public void checkNegativeScore(){
        assert (getMarkResult(101).equals("no mark result"));
    }

    @Test
    public void checkScore0() {
        assert (getMarkResult(0).equals("2"));
    }

    @Test
    public void checkScore35() {
        assert (getMarkResult(35).equals("2"));
    }

    @Test
    public void checkScore36() {
        assert (getMarkResult(36).equals("3"));
    }

    @Test
    public void checkScore56() {
        assert (getMarkResult(56).equals("3"));
    }

    @Test
    public void checkScore57() {
        assert (getMarkResult(57).equals("4"));
    }

    @Test
    public void checkScore71() {
        assert (getMarkResult(71).equals("4"));
    }

    @Test
    public void checkScore72() {
        assert (getMarkResult(72).equals("5"));
    }

    @Test
    public void checkScore100() {
        assert (getMarkResult(100).equals("5"));
    }
}