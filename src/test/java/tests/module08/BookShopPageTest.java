package tests.module08;

import org.junit.Test;
import pages.module08.BookSearchPage;
import pages.module08.BookShopPage;
import tests.AbstractTest;

public class BookShopPageTest extends AbstractTest{
    private final String urlMain = "http://qajava.skillbox.ru/index.html";
    private final String urlSearch = "http://qajava.skillbox.ru/search.html";
    BookShopPage bookShopPage;
    BookSearchPage bookSearchPage;
    private void getPage(String url){
        driver.get(url);
    }

    @Test
    public void isDisplayedElementsFirstTest(){
        bookShopPage = new BookShopPage(driver);
        getPage(urlMain);
        bookShopPage.isDisplayedElementsFirst();
        bookSearchPage = new BookSearchPage(driver);
        getPage(urlSearch);
        bookSearchPage.isDisplayedElementsFirst();
    }

    @Test
    public void isDisplayedElementsSecondTest(){
        bookShopPage = new BookShopPage(driver);
        getPage(urlMain);
        bookShopPage.isDisplayedElementsSecond();
        bookSearchPage = new BookSearchPage(driver);
        getPage(urlSearch);
        bookSearchPage.isDisplayedElementsSecond();
    }
}
