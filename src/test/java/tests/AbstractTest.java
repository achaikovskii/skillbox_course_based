package tests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class AbstractTest {
    public static WebDriver driver;

    public static void openBrowser(){
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver,10);
    }

    public static void tearDown(){
        driver.close();
        driver.quit();
        driver = null;
    }


    @Before
    public void setUp() {
        openBrowser();
    }

    @After
    public void clearAll(){ tearDown(); }

}
