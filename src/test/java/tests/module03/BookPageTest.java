package tests.module03;

import org.junit.Test;
import pages.module03.BookPage;
import tests.AbstractTest;

public class BookPageTest extends AbstractTest {
    BookPage bookPage;
    private final int correctCountOfBooks = 15;

    private void getPage(){
        driver.get("http://qajava.skillbox.ru/");
    }

    @Test
    public void findElements(){
        getPage();
        bookPage = new BookPage(driver);
        bookPage.checkGeneralLocators();
    }

    @Test
    public void countLocatorsOfInfoBooksTest(){
        getPage();
        bookPage = new BookPage(driver);
        assert (bookPage.countInfoBooks() == correctCountOfBooks) : "Actual Result: "+bookPage.countInfoBooks()+", Expected Result "+correctCountOfBooks+"";
    }
}
