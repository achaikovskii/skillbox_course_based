package tests.module03;

import org.junit.Test;
import pages.module03.CinemaPage;
import tests.AbstractTest;

public class CinemaPagetest extends AbstractTest{
    CinemaPage cinemaPage;

    private void getPage(){
        driver.get("https://lm.skillbox.ru/qa_tester/module07/practice3/");
    }

    @Test
    public void lovesTitlesTest(){
        cinemaPage = new CinemaPage(driver);
        getPage();
        cinemaPage.checkInputFilmsSerials();
    }

    @Test
    public void checkEmptyFields(){
        cinemaPage = new CinemaPage(driver);
        getPage();
        cinemaPage.checkInputFilmsSerialsEmpty();
    }

    @Test
    public void checkCountOfCheckboxes(){
        cinemaPage = new CinemaPage(driver);
        getPage();
        cinemaPage.checkCountCheckboxes();
    }
}
