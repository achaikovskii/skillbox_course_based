package tests.module03;

import org.junit.Test;
import pages.module03.ShoesPage;
import tests.AbstractTest;

public class ShoesPageTest extends AbstractTest{
    ShoesPage shoesPage;
    private final String correctSize = "34";
    private final String incorrectSize = "33";

    private void getPage(){
        driver.get("https://lm.skillbox.ru/qa_tester/module03/practice1/");
    }

    @Test
    public void inputNegativeSizeTest(){
        getPage();
        shoesPage = new ShoesPage(driver);
        shoesPage.setSizeField(incorrectSize);
        shoesPage.clickFindButton();
        shoesPage.checkErrorText();
    }

    @Test
    public void inputCorrectSizTest(){
        getPage();
        shoesPage = new ShoesPage((driver));
        shoesPage.setSizeField(correctSize);
        shoesPage.clickFindButton();
        shoesPage.checkSuccessText();
    }
}
