package tests.module09;

import org.junit.Test;
import pages.module09.TaxiPage;
import tests.AbstractTest;

public class TaxiPageTest extends AbstractTest {
    TaxiPage taxiPage;

    private void getPage(){
        driver.get("https://lm.skillbox.cc/qa_tester/module04/practice1/");
    }

    @Test
    public void isDisplayedElementsTest(){
        taxiPage = new TaxiPage(driver);
        getPage();
        taxiPage.isDisplayedElements();
    }
}
