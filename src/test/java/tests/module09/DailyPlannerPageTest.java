package tests.module09;

import org.junit.Test;
import pages.module09.DailyPlannerPage;
import tests.AbstractTest;

public class DailyPlannerPageTest extends AbstractTest{
    DailyPlannerPage dailyPlannerPage;

    private void getPage(){
        driver.get("http://qa.skillbox.ru/module15/bignotes/#/statistic");
    }

    @Test
    public void smokeTest(){
        dailyPlannerPage = new DailyPlannerPage(driver);
        getPage();
        dailyPlannerPage.smoke();
    }
}
