package tests.module05;

import org.junit.Test;
import pages.module05.CinemaPage;
import tests.AbstractTest;

public class CinemaPageTest extends AbstractTest{
    CinemaPage cinemaPage;

    private void getPage(){
        driver.get("https://lm.skillbox.cc/qa_tester/module06/register/");
    }

    @Test
    public void successRegistration(){
        cinemaPage = new CinemaPage(driver);
        getPage();
        cinemaPage.checkSuccessRegistration();
    }

    @Test
    public void errorRegistrationName(){
        cinemaPage = new CinemaPage(driver);
        getPage();
        cinemaPage.checkEmptyRegistration();
    }
}
