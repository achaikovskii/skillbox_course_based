package tests.module10;

import org.junit.Test;
import pages.module10.CinemaPage;
import tests.AbstractTest;

public class CinemaPageTest extends AbstractTest {
    CinemaPage cinemaPage;

    private void getPage(){
        driver.get("http://qa.skillbox.ru/module19/");
    }

    @Test
    public void isDisplayedElementsTest(){
        cinemaPage = new CinemaPage(driver);
        getPage();
        cinemaPage.isDisplayedElements();
    }
}
