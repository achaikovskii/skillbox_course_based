package tests.module10;

import org.junit.Test;
import pages.module10.BookBasketPage;
import pages.module10.BookPage;
import tests.AbstractTest;

public class BookPageTest extends AbstractTest {
    private final String urlBookPage = "http://qajava.skillbox.ru/index.html";
    private final String urlBookBasketPage = "http://qajava.skillbox.ru/checkout.html";
    BookPage bookPage;
    BookBasketPage bookBasketPage;

    private void getPage(String url){ driver.get(url); }

    @Test
    public void isDisplayedElementsMainPageTest(){
        bookPage = new BookPage(driver);
        getPage(urlBookPage);
        bookPage.isDisplayedElements();

    }

    @Test
    public void isDisplayedElementsBasketPageTest() {
        bookBasketPage = new BookBasketPage(driver);
        getPage(urlBookBasketPage);
        bookBasketPage.isDisplayedElements();
    }
}
