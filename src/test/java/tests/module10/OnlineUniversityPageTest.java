package tests.module10;

import org.junit.Test;
import pages.module10.OnlineUniversityPage;
import tests.AbstractTest;

public class OnlineUniversityPageTest extends AbstractTest {
    OnlineUniversityPage onlineUniversityPage;

    private void getPage(){
        driver.get("http://qa.skillbox.ru/module16/maincatalog/");
    }

    @Test
    public void isDisplayedElementsTest(){
        onlineUniversityPage = new OnlineUniversityPage(driver);
        getPage();
        onlineUniversityPage.isDisplayedElements();
    }
}
