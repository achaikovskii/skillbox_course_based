package tests.module07;

import org.junit.Test;
import pages.module07.KittyPage;
import tests.AbstractTest;

public class KittyPageTest extends AbstractTest{
    KittyPage kittyPage;

    public void getPage(){
        driver.get("http://qajava.skillbox.ru/module04/lesson3/index.html");
    }

    @Test
    public void checkResultWithAllFields(){
        kittyPage = new KittyPage(driver);
        getPage();
        kittyPage.setAllFields();
        kittyPage.assertResults();
    }
}
