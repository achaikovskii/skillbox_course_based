package pages.module02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WelcomePageAdvanced {
    private final String name = "Вася";
    private final String email = "test@test.com";
    private final String phone = "+79291112233";
    WebDriver driver;

    public WelcomePageAdvanced(WebDriver driver) {
        this.driver = driver;
    }

    public void setWithFullData() {
        setName();
        setEmail();
        setPhone();
        clickButtonSubmit();
    }

    public void setWithoutName() {
        setEmail();
        setPhone();
        clickButtonSubmit();
    }

    public void setWithoutEmail() {
        setName();
        setPhone();
        clickButtonSubmit();
    }

    public void setWithoutPhone() {
        setName();
        setEmail();
        clickButtonSubmit();
    }

    public void setNoData(){
        clickButtonSubmit();
    }

    private void setName() {
        driver.findElement(By.cssSelector("[name='name']")).sendKeys(name);
    }

    private void setEmail() {
        driver.findElement(By.cssSelector("[name='email']")).sendKeys(email);
    }

    private void setPhone() {
        driver.findElement(By.cssSelector("[name='phone']")).sendKeys(phone);
    }

    private void clickButtonSubmit() {
        driver.findElement(By.xpath("//button[@class='custom-form__button button']")).click();
    }

    public void checkFullField() {
        assert (driver.findElement(By.cssSelector("p.start-screen__res.container")).getText().equals("Здравствуйте, "+name+".\n" +
                "На вашу почту ("+email+") отправлено письмо.\n" +
                "Наш сотрудник свяжется с вами по телефону: "+phone+"."));
    }

    public void checkNameFieldEmpty() {
        assert (driver.findElement(By.cssSelector("p.start-screen__res.container")).getText().equals("Здравствуйте, .\n" +
                "На вашу почту ("+email+") отправлено письмо.\n" +
                "Наш сотрудник свяжется с вами по телефону: "+phone+"."));
    }

    public void checkEmailFieldEmpty() {
        assert (driver.findElement(By.cssSelector("p.start-screen__res.container")).getText().equals("Здравствуйте, "+name+".\n" +
                "На вашу почту () отправлено письмо.\n" +
                "Наш сотрудник свяжется с вами по телефону: "+phone+"."));
    }

    public void checkPhoneFieldEmpty() {
        assert (driver.findElement(By.cssSelector("p.start-screen__res.container")).getText().equals("Здравствуйте, "+name+".\n" +
                "На вашу почту ("+email+") отправлено письмо.\n" +
                "Наш сотрудник свяжется с вами по телефону: ."));
    }

    public void checkEmptyAllData(){
        assert (driver.findElement(By.cssSelector("p.start-screen__res.container")).getText().equals("Здравствуйте, .\n" +
                "На вашу почту () отправлено письмо.\n" +
                "Наш сотрудник свяжется с вами по телефону: ."));
    }
}