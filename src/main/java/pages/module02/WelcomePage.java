package pages.module02;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WelcomePage {
    private final String name = "Вася";
    WebDriver driver;
    public WelcomePage(WebDriver driver){ this.driver = driver; }

    public void goToWelcome(){
        setName();
        clickButtonSubmit();
    }

    public void checkWelcomeString(){
        assert (driver.findElement(By.xpath("//p[@class='start-screen__res container']")).getText().equals("Привет, " + name + "!"));
    }

    public void checkWelcomeStringWithoutTextInField(){
        clickButtonSubmit();
        assert (driver.findElement(By.xpath("//p[@class='start-screen__res container']")).getText().equals("Привет, !"));
    }

    private void setName(){
        driver.findElement(By.xpath("//input[@name='name']")).sendKeys(name);
    }

    private void clickButtonSubmit(){
        driver.findElement(By.xpath("//button[@class='custom-form__button button']")).click();
    }

}
