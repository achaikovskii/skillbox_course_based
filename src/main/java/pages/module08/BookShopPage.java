package pages.module08;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookShopPage {
    //locators for first test
    private final By aboutInfoLocator = By.cssSelector("[test-info='about-us']");
    private final By emptyTagLinkLocator = By.cssSelector("a[href='']");
    private final By tagClassLocator = By.cssSelector("div.book-price");
    private final By buttonTagAndClassStartedLocator = By.cssSelector("button[class^='book']");
    private final By allClassFinishLocator = By.cssSelector("*[class$='main']");
    private final By aContainsClassLocator = By.cssSelector("a[class*='menu']");

    //locators for second test
    private final By nextOfFooterDivsLocator = By.cssSelector("footer>div");
    private final By anyNextElementOfElementLocator = By.cssSelector("#genres>:first-child");
    WebDriver driver;
    public BookShopPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElementsFirst(){
        driver.findElement(aboutInfoLocator).isDisplayed();
        driver.findElement(emptyTagLinkLocator).isDisplayed();
        driver.findElement(tagClassLocator).isDisplayed();
        driver.findElement(buttonTagAndClassStartedLocator).isDisplayed();
        driver.findElement(allClassFinishLocator).isDisplayed();
        driver.findElement(aContainsClassLocator).isDisplayed();
    }

    public void isDisplayedElementsSecond(){
        driver.findElement(nextOfFooterDivsLocator).isDisplayed();
        driver.findElement(anyNextElementOfElementLocator).isDisplayed();
    }


}
