package pages.module08;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SocksPage {
    private final By firstTagLocator = By.cssSelector(".important-section-block>h1:first-child");
    private final By lastTagLocator = By.cssSelector("#login-form>p:last-child");
    private final By thirdElementLocator = By.cssSelector("body>*:nth-child(3)");
    private final By firstTypeElementLocator = By.cssSelector(".footer__menuList>a:nth-of-type(1)");
    private final By strongLocator = By.cssSelector(".footer__menu>:first-child>a:nth-of-type(1)");
    WebDriver driver;
    public SocksPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElements(){
        driver.findElement(firstTagLocator).isDisplayed();
        driver.findElement(lastTagLocator).isDisplayed();
        driver.findElement(thirdElementLocator).isDisplayed();
        driver.findElement(firstTypeElementLocator).isDisplayed();
        driver.findElement(strongLocator).isDisplayed();
    }
}
