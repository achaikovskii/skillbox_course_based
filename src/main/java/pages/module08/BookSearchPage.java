package pages.module08;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookSearchPage {
    private final By allOptionSelectedLocator = By.cssSelector("[selected]");
    private final By anyNextElementsLocator = By.cssSelector(".filter-container>*");
    WebDriver driver;
    public BookSearchPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElementsFirst(){
        driver.findElement(allOptionSelectedLocator).isDisplayed();
    }

    public void isDisplayedElementsSecond(){
        driver.findElement(anyNextElementsLocator).isDisplayed();
    }
}
