package pages.module07;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class KittyPage {
    private final String fio = "John Smith";
    private final String street = "Beverly Hills";
    private final String house = "90210";
    private final String flat = "7";
    private final String date = "10.10.2023";

    private final By fioLocator = By.cssSelector(".form-input.fio");
    private final By streetLocator = By.cssSelector(".data.form-input.street");
    private final By houseLocator = By.cssSelector(".form-input.house");
    private final By flatLocator = By.cssSelector(".form-input.flat");
    private final By dateLocator = By.cssSelector(".form-input.date");
    private final By submitBtnlocator = By.cssSelector(".form-submit");

    private final By fioResultLocator = By.cssSelector(".show-fio");
    private final By streetResultLocator = By.cssSelector(".show-street");
    private final By houseResultLocator = By.cssSelector(".show-house");
    private final By flatResultLocator = By.cssSelector(".show-flat");
    private final By dateResultLocator = By.cssSelector(".show-date");
    WebDriver driver;

    public KittyPage(WebDriver driver) { this.driver = driver; }

    public void setAllFields(){
        setFIO(fio);
        setStreet(street);
        setHouse(house);
        setFlat(flat);
        setDate(date);
        clickSubmit();
    }

    public void assertResults(){
        assert (driver.findElement(fioResultLocator).getText().contains(fio)) : "Incorrect Name in the result";
        assert (driver.findElement(streetResultLocator).getText().contains(street)) : "Incorrect street in the result";
        assert (driver.findElement(houseResultLocator).getText().contains(house)) : "Incorrect house in the result";
        assert (driver.findElement(flatResultLocator).getText().contains(flat)) : "Incorrect flat in the result";
        assert (driver.findElement(dateResultLocator).getText().contains(date)) : "Incorrect date in the result";
    }

    private void setFIO(String fio){ driver.findElement(fioLocator).sendKeys(fio); }
    private void setStreet(String street){ driver.findElement(streetLocator).sendKeys(street); }
    private void setHouse(String house){ driver.findElement(houseLocator).sendKeys(house); }
    private void setFlat(String flat){ driver.findElement(flatLocator).sendKeys(flat); }
    private void setDate(String date){ driver.findElement(dateLocator).sendKeys(date); }
    private void clickSubmit(){ driver.findElement(submitBtnlocator).click(); }
}
