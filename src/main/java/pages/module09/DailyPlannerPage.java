package pages.module09;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DailyPlannerPage {
    private final By noteFromCenter = By.cssSelector("button.articlePreview__link");
    private final By titleNoteInListLocator = By.cssSelector(".vb-content>div:nth-child(1) p:nth-child(2)");
    private final By textNoteInListLocator = By.cssSelector(".vb-content>div:nth-child(1) p:nth-child(3)");
    private final By deleteButtonLocator = By.cssSelector(".pageArticle__button:nth-of-type(2)");
    private final By firstNoteInListLocator = By.cssSelector(".articlePreview__link:nth-of-type(1)");
    WebDriver driver;
    public DailyPlannerPage(WebDriver driver) { this.driver = driver; }

    public void smoke(){
        WebDriverWait wait = new WebDriverWait(driver, 2);
        clickNoteCenter();
        checkTitleNoteInList();
        checkTextNoteInList();
        clickDeleteButton();
        clickFirstNoteInList();
        sleep(1000);
        clickDeleteButton();
        isUndisplayedFirstOnList();
    }

    private void clickNoteCenter(){
        driver.findElement(noteFromCenter).click();
    }
    private void checkTitleNoteInList(){
        sleep(1000);
        assert (driver.findElement(titleNoteInListLocator).getText().equals("План на следующий месяц")):"Text in title has difference"; }
    private void checkTextNoteInList(){
        sleep(1000);
        assert (driver.findElement(textNoteInListLocator).getText().equals("Прочитать книгу «Искусство цвета».")):"Text in description has difference"; }
    private void clickDeleteButton(){ driver.findElement(deleteButtonLocator).click(); }
    private void clickFirstNoteInList(){ driver.findElement(firstNoteInListLocator).click(); }
    private void isUndisplayedFirstOnList(){ assert (driver.findElements(firstNoteInListLocator).size() == 0);}
    private void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
