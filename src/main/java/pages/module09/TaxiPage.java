package pages.module09;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TaxiPage {
    private final By firstLocator = By.cssSelector("input:not(id)");
    private final By secondLocator = By.cssSelector("p[class^='form']:not([class$='error'])");
    private final By thirdLocator = By.cssSelector(".form-inner .form-row:nth-of-type(1)");
    WebDriver driver;
    public TaxiPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElements(){
        isDisplayedFirst();
        isDisplayedSecond();
        isDisplayedThird();
    }

    private void isDisplayedFirst() { driver.findElement(firstLocator).isDisplayed(); }
    private void isDisplayedSecond() { driver.findElement(secondLocator).isDisplayed(); }
    private void isDisplayedThird() { driver.findElement(thirdLocator).isDisplayed(); }
}
