package pages.module05;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CinemaPage {
    private final String name = "Aleksandr";
    private final String email = "skillbox@test.ru";
    private final String password = "qwerty!123";
    private final By nameLocator = By.cssSelector("[name='name']");
    private final By emailLocator = By.cssSelector("[name='email']");
    private final By passwordLocator = By.cssSelector("[name='password']");
    private final By submitLocator = By.cssSelector(".form-submit");
    private final By resultText = By.cssSelector(".form-result");
    private final By errorText = By.cssSelector(".form>.form-error");
    WebDriver driver;

    public CinemaPage(WebDriver driver) { this.driver = driver; }

    public void checkSuccessRegistration(){
        setNameField(name);
        setEmailField(email);
        setPasswordField(password);
        clickSubmit();
        checkSuccessMessage();
    }

    public void checkEmptyRegistration(){
        clickSubmit();
        checkErrorMessage();
    }

    private void setNameField(String name){
        driver.findElement(nameLocator).sendKeys(name);
    }

    private void setEmailField(String email){
        driver.findElement(emailLocator).sendKeys(email);
    }

    private void setPasswordField(String password){
        driver.findElement(passwordLocator).sendKeys(password);
    }

    private void clickSubmit(){
        driver.findElement(submitLocator).click();
    }

    private void checkSuccessMessage(){
        assert (driver.findElement(resultText).getText().equals("Вам на почту " + email + " отправлено письмо"));
    }

    private void checkErrorMessage(){
        assert (driver.findElement(errorText).getText().equals("Введите имя"));
    }
}
