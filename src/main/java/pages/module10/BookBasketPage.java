package pages.module10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookBasketPage {
    private final By totalAmountOfBasketLocator = By.xpath("//div[@id='total']");
    private final By titleYourOrderLocator = By.xpath("(//div[@class='order-info']/div)[1]");
    WebDriver driver;
    public BookBasketPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElements(){
        isDisplayedTotalAmountOfBasket();
        isDisplayedTitleYourOrder();
    }

    private void isDisplayedTotalAmountOfBasket(){ driver.findElement(totalAmountOfBasketLocator).isDisplayed(); }
    private void isDisplayedTitleYourOrder(){ driver.findElement(titleYourOrderLocator).isDisplayed(); }
}
