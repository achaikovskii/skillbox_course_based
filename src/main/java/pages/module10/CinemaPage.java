package pages.module10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CinemaPage {
    private final By prevLocator = By.xpath("//span[@class='da-arrows-prev']");
    private final By knowMoreButtonLocator = By.xpath("//a[@class='da-link button']");
    private final By nonActiveFiltersLocator = By.xpath("//li[@class='filter']");
    private final By classStartedFromButtonLocator = By.xpath("//*[starts-with(@class, 'button')]");
    private final By subscribeNowButtonLocator = By.xpath("//a[text()='Подписаться сейчас']");
    private final By allClientsPhotoLocator = By.xpath("//ul[@id='clint-slider']//img");


    WebDriver driver;
    public CinemaPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElements(){
        isDisplayedPrevLink();
        isDisplayedKnowMoreButton();
        isDisplayedNonActiveFilters();
        isDisplayedClassStartedFromButton();
        isDisplayedSubscribeNowButton();
        isDisplayedAllClientsPhoto();
    }

    private void isDisplayedPrevLink(){ driver.findElement(prevLocator).isDisplayed(); }
    private void isDisplayedKnowMoreButton(){ driver.findElement(knowMoreButtonLocator).isDisplayed(); }
    private void isDisplayedNonActiveFilters(){ driver.findElement(nonActiveFiltersLocator).isDisplayed(); }
    private void isDisplayedClassStartedFromButton(){ driver.findElement(classStartedFromButtonLocator).isDisplayed(); }
    private void isDisplayedSubscribeNowButton(){ driver.findElement(subscribeNowButtonLocator).isDisplayed(); }
    private void isDisplayedAllClientsPhoto(){ driver.findElement(allClientsPhotoLocator).isDisplayed(); }
}
