package pages.module10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookPage {
    private final By aboutShopLocator = By.xpath("//a[@test-info='about-us']");
    private final By bestsellersTitleLocator = By.xpath("//li/a[text()='Бестселлеры']");
    private final By searchFieldLocator = By.xpath("//input[starts-with(@name, 'search')]");

    WebDriver driver;
    public BookPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElements(){
        isDisplayedAboutShop();
        isDisplayedBestsellersTitle();
        isDisplayedSearchField();
    }

    private void isDisplayedAboutShop(){ driver.findElement(aboutShopLocator).isDisplayed(); }
    private void isDisplayedBestsellersTitle(){ driver.findElement(bestsellersTitleLocator).isDisplayed(); }
    private void isDisplayedSearchField(){ driver.findElement(searchFieldLocator).isDisplayed(); }
}
