package pages.module10;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OnlineUniversityPage {
    private final By titleOfFiveCourseLocator = By.xpath("(//*[@class='pageCreate__cards']//span[@class='baseCard__title'])[5]");
    private final By lastPeriodsOfEducationLocator = By.xpath("(//*[@class='pageCreate__cards']//div[@class='baseCard__conditions'])[last()]");
    private final By allDivParentLocator = By.xpath("//a[@href='#']/parent::div");
    private final By fiveOfAllParentLocator = By.xpath("(//a[@href='#']/parent::div)[5]");
    private final By allAncestorOfElementLocator = By.xpath("//div[@class='pageCreate__title']/ancestor::*");
    WebDriver driver;
    public OnlineUniversityPage(WebDriver driver) { this.driver = driver; }

    public void isDisplayedElements(){
        isDisplayedTitleOfFiveCourse();
        isDisplayedLastPeriodsOfEducation();
        isDisplayedAllDivParent();
        isDisplayedFiveOfAllParent();
        isDisplayedAllAncestorOfElement();
    }

    private void isDisplayedTitleOfFiveCourse(){
        driver.findElement(titleOfFiveCourseLocator).isDisplayed();
    }

    private void isDisplayedLastPeriodsOfEducation(){
        driver.findElement(lastPeriodsOfEducationLocator).isDisplayed();
    }

    private void isDisplayedAllDivParent(){
        driver.findElement(allDivParentLocator).isDisplayed();
    }

    private void isDisplayedFiveOfAllParent(){
        driver.findElement(fiveOfAllParentLocator).isDisplayed();
    }

    private void isDisplayedAllAncestorOfElement(){
        driver.findElement(allAncestorOfElementLocator).isDisplayed();
    }

}
