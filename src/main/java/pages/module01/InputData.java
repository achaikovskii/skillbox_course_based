package pages.module01;

public class InputData {
    private static final String name = "Александр";
    private static final String surName = "Чайковский";
    private static final String correctEmail = "test@test.com";
    private static final String correctPassword = "Qwerty123456!";

    public static String getName(){ return name; }
    public static String getSurName(){ return surName; }
    public static String getCorrectEmail(){ return correctEmail; }
    public static String getCorrectPassword(){ return correctPassword; }
}
