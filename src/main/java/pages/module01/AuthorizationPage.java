package pages.module01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AuthorizationPage {
    private final WebDriver driver;
    public AuthorizationPage(WebDriver driver){
        this.driver = driver;
    }

    public void inputAuthorizationForm() {
        setName();
        setSurName();
        setEmail();
        setCorrectPassword();
        setConfirmCorrectPassword();
        agreeConditions();
        clickSubmit();
    }

    public void inputAuthorizationFormWithoutEmail(){
        setName();
        setSurName();
        setCorrectPassword();
        setConfirmCorrectPassword();
        agreeConditions();
        clickSubmit();
    }

    private void setName(){ driver.findElement(By.xpath("//input[@id='name']")).sendKeys(InputData.getName()); }
    private void setSurName(){ driver.findElement(By.xpath("//input[@id='surname']")).sendKeys(InputData.getSurName()); }
    private void setEmail(){ driver.findElement(By.xpath("//input[@id='email']")).sendKeys(InputData.getCorrectEmail()); }
    private void setCorrectPassword(){ driver.findElement(By.xpath("//input[@id='password']")).sendKeys(InputData.getCorrectPassword()); }
    private void setConfirmCorrectPassword(){ driver.findElement(By.xpath("//input[@id='confirm_password']")).sendKeys(InputData.getCorrectPassword()); }
    private void agreeConditions(){ driver.findElement(By.xpath("//label[@class='form-label-agree']")).click(); }
    private void clickSubmit(){ driver.findElement(By.xpath("//button[@class='form-submit']")).click(); }

    public void checkMessageInSuccessMessage(){
        driver.findElement(By.xpath("//h3[@class='form-title result']")).isDisplayed();
        assert (driver.findElement(By.xpath("//h3[@class='form-title result']")).getText().equals(InputData.getName() + ", спасибо за регистрацию!"));
    }

    public void checkEmailError(){
        driver.findElement(By.xpath("//p[@class='form-row']/p[@class='form-error']")).isDisplayed();
        assert (driver.findElement(By.xpath("//p[@class='form-row']/p[@class='form-error']")).getText().equals("Email не удовлетворяет условиям"));
    }

    public void checkErrorOfFieldsEmpty(){
        driver.findElement(By.xpath("//div[@class='form-errors']/p[@class='form-error']")).isDisplayed();
        assert (driver.findElement(By.xpath("//div[@class='form-errors']/p[@class='form-error']")).getText().equals("Неверно заполнены поля"));
    }
}