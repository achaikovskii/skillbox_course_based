package pages.module04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SocksPage {
    WebDriver driver;
    private By emailFieldLocator = By.name("email");
    private By passwordFieldLocator = By.id("password");
    private By signInButtonLocator = By.tagName("button");
    private By errorMessageLocator = By.cssSelector("pre#error");
    public SocksPage (WebDriver driver) { this.driver = driver; }

    public void checkErrorMessage(){
        setEmailField();
        setPasswordField();
        clickSignInButton();
        isDisplayedErrorMessage();
        getErrorMessage();
    }

    private void setEmailField(){
        driver.findElement(emailFieldLocator).sendKeys("@");
    }

    private void setPasswordField(){
        driver.findElement(passwordFieldLocator).sendKeys("123");
    }

    private void clickSignInButton(){
        driver.findElement(signInButtonLocator).click();
    }

    private void isDisplayedErrorMessage(){
        driver.findElement(errorMessageLocator).isDisplayed();
    }

    private void getErrorMessage(){
        assert (driver.findElement(errorMessageLocator).getText().equals("Некорректный email или пароль"));
    }
}
