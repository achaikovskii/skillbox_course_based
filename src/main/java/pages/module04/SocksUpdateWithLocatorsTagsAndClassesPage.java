package pages.module04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SocksUpdateWithLocatorsTagsAndClassesPage {
    private By emailFieldLocator = By.cssSelector("input.form-input.input-data");
    private By passwordFieldLocator = By.cssSelector("input.form-input.password");
    private By signInButtonLocator = By.cssSelector("button.form-submit");
    private By errorMessageLocator = By.cssSelector("pre.form-error-password-email");
    WebDriver driver;

    public SocksUpdateWithLocatorsTagsAndClassesPage(WebDriver driver){ this.driver = driver; }


    public void checkErrorMessage(){
        setEmailField();
        setPasswordField();
        clickSignInButton();
        isDisplayedErrorMessage();
        getErrorMessage();
    }

    private void setEmailField(){
        driver.findElement(emailFieldLocator).sendKeys("@");
    }

    private void setPasswordField(){
        driver.findElement(passwordFieldLocator).sendKeys("123");
    }

    private void clickSignInButton(){
        driver.findElement(signInButtonLocator).click();
    }

    private void isDisplayedErrorMessage(){
        driver.findElement(errorMessageLocator).isDisplayed();
    }

    private void getErrorMessage(){
        assert (driver.findElement(errorMessageLocator).getText().equals("Некорректный email или пароль"));
    }

}
