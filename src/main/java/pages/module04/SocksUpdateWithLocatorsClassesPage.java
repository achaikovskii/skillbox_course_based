package pages.module04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SocksUpdateWithLocatorsClassesPage {
    private By emailFieldLocator = By.cssSelector(".form-input.input-data");
    private By passwordFieldLocator = By.cssSelector(".password");
    private By signInButtonLocator = By.cssSelector(".form-submit");
    private By errorMessageLocator = By.cssSelector(".form-error-password-email");
    WebDriver driver;

    public SocksUpdateWithLocatorsClassesPage(WebDriver driver){ this.driver = driver; }


    public void checkErrorMessage(){
        setEmailField();
        setPasswordField();
        clickSignInButton();
        isDisplayedErrorMessage();
        getErrorMessage();
    }

    private void setEmailField(){
        driver.findElement(emailFieldLocator).sendKeys("@");
    }

    private void setPasswordField(){
        driver.findElement(passwordFieldLocator).sendKeys("123");
    }

    private void clickSignInButton(){
        driver.findElement(signInButtonLocator).click();
    }

    private void isDisplayedErrorMessage(){
        driver.findElement(errorMessageLocator).isDisplayed();
    }

    private void getErrorMessage(){
        assert (driver.findElement(errorMessageLocator).getText().equals("Некорректный email или пароль"));
    }

}
