package pages.module04;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SocksUpdateWithLocatorsIDPage {
    private By emailFieldLocator = By.cssSelector("#email");
    private By passwordFieldLocator = By.cssSelector("#password");
    private By signInButtonLocator = By.cssSelector("#submit");
    private By errorMessageLocator = By.cssSelector("#error");
    WebDriver driver;

    public SocksUpdateWithLocatorsIDPage(WebDriver driver){ this.driver = driver; }


    public void checkErrorMessage(){
        setEmailField();
        setPasswordField();
        clickSignInButton();
        isDisplayedErrorMessage();
        getErrorMessage();
    }

    private void setEmailField(){
        driver.findElement(emailFieldLocator).sendKeys("@");
    }

    private void setPasswordField(){
        driver.findElement(passwordFieldLocator).sendKeys("123");
    }

    private void clickSignInButton(){
        driver.findElement(signInButtonLocator).click();
    }

    private void isDisplayedErrorMessage(){
        driver.findElement(errorMessageLocator).isDisplayed();
    }

    private void getErrorMessage(){
        assert (driver.findElement(errorMessageLocator).getText().equals("Некорректный email или пароль"));
    }

}
