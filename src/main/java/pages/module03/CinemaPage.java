package pages.module03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CinemaPage {
    private final WebDriver driver;
    private final String loveFilm = "Hot heads";
    private final String loveSerial = "Silicon Valley";
    private final int correctCountOfCheckboxes = 4;
    public CinemaPage(WebDriver driver){
        this.driver = driver;
    }

    private void setFilmField(String film){
        driver.findElement(By.cssSelector("input#films")).sendKeys(film);
    }

    private void setSerialsField(String serial){
        driver.findElement(By.cssSelector("input#serials")).sendKeys(serial);
    }

    private void clickSaveButton(){
        driver.findElement(By.cssSelector("input#save")).click();
    }

    private void goToTwoForm(){
        driver.findElement(By.cssSelector("input#two")).click();
    }

    private void clickOkButton(){
        driver.findElement(By.cssSelector("input#ok")).click();
    }

    public void checkInputFilmsSerials(){
        setFilmField(loveFilm);
        setSerialsField(loveSerial);
        clickSaveButton();
        goToTwoForm();
        clickSaveButton();
        clickOkButton();
        assert (driver.findElement(By.cssSelector(".result__text#best_films")).getText().equals(loveFilm));
        assert (driver.findElement(By.cssSelector(".result__text#best_serials")).getText().equals(loveSerial));
    }

    public void checkInputFilmsSerialsEmpty(){
        setFilmField("");
        setSerialsField("");
        clickSaveButton();
        goToTwoForm();
        clickSaveButton();
        clickOkButton();
        assert (driver.findElement(By.cssSelector(".result__text#best_films")).getText().equals(""));
        assert (driver.findElement(By.cssSelector(".result__text#best_serials")).getText().equals(""));
    }

    public void checkCountCheckboxes(){
        assert (driver.findElements(By.className("fake-checkbox")).size() == correctCountOfCheckboxes);
    }
}
