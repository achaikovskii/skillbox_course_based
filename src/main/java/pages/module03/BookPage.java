package pages.module03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookPage {
    private final WebDriver driver;
    public BookPage(WebDriver driver){
        this.driver = driver;
    }

    public void checkGeneralLocators(){
        isDisplayedFeedBack();
        isDisplayedPresales();
        isDisplayedAddToBasketButton();
        isDisplayedCountOfItemsInButton();
        isDisplayedGenres();
        isDisplayedInputSearch();
    }

    private void isDisplayedFeedBack(){
        driver.findElement(By.linkText("Обратная связь")).isDisplayed();
    }

    private void isDisplayedPresales(){
        driver.findElement(By.linkText("Предзаказы")).isDisplayed();
    }

    private void isDisplayedAddToBasketButton(){
        driver.findElement(By.className("book-add")).isDisplayed();
    }

    private void isDisplayedCountOfItemsInButton(){
        driver.findElement(By.id("cart_count")).isDisplayed();
    }

    private void isDisplayedGenres(){
        driver.findElement(By.cssSelector("li#genres")).isDisplayed();
    }

    private void isDisplayedInputSearch(){
        driver.findElement(By.id("search-input")).isDisplayed();
    }

    public int countInfoBooks(){
        return driver.findElements(By.className("book-info")).size();
    }
}
