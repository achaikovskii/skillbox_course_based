package pages.module03;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShoesPage {
    private final WebDriver driver;
    private final String successText = "В нашем магазине есть обувь вашего размера";
    private final String errorText = "В нашем магазине нет обуви вашего размера";
    public ShoesPage(WebDriver driver){
        this.driver = driver;
    }

    public void setSizeField(String size){
        driver.findElement(By.id("size")).sendKeys(size);
    }

    public void clickFindButton(){
        driver.findElement(By.cssSelector("button#check-size-button")).click();
    }

    public  void checkErrorText(){
        driver.findElement(By.id("size-error")).isDisplayed();
        assert (driver.findElement(By.id("size-error")).getText().equals(errorText)) : "Ошибка поиска не отобразилась";
    }

    public  void checkSuccessText(){
        driver.findElement(By.id("size-success")).isDisplayed();
        assert (driver.findElement(By.id("size-success")).getText().equals(successText)) : "Ошибка поиска";
    }
}
