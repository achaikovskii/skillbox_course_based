## Automation tests examples with Java + Selenium

## Description
Это проект выполненных практических работ по курсу skillbox (Автоматизация тестирования на Java)
Требования для выполнения заданий находятся в папке - Description_of_tasks

This is a project of completed practical work on the skillbox course (Java Testing Automation)
The requirements for completing tasks are located in the folder - Description_of_tasks
